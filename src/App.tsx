import React from "react";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import EmployeeList from "./pages/EmployeeList";
import EmployeeManagement from "./pages/EmployeeManagement";
import ProjectListPage from "./pages/ProjectListPage";
import NotFoundPage from "./pages/NotFoundPage";
import Topbar from "./components/Topbar/Topbar";
import Navbar from "./components/Navbar";
import ExampleEmployeeList from "./pages/ExampleEmployeeList";
import LeftSidebar from "./components/LeftSidebar";

function App() {
    return (
        <>
            <BrowserRouter>
                <ToastContainer autoClose={3000} hideProgressBar={true} />
                <Topbar />
                {/*<LeftSidebar/>*/}
                <Navbar />
                <Switch>
                    <Route exact path="/" component={ExampleEmployeeList} />
                    <Route path="/employees" component={EmployeeList} />
                    <Route path="/employee/:id" component={EmployeeManagement} />
                    <Route path="/employee" component={EmployeeManagement} />
                    <Route path="/project" component={ProjectListPage} />
                    <Route component={NotFoundPage} />
                </Switch>
            </BrowserRouter>
        </>
    );
}

export default App;