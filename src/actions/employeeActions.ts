import dispatcher from '../appFluxDispatcher';
import {ajaxDelete, ajaxGet, ajaxPost} from '../utils/apiUtils';
import { Employee } from '../domain/employee';
import actionTypes from "./actionTypes";

export function saveEmployee(employee : Employee) {
    return ajaxPost("http://localhost:8080/api/employee", employee).then(savedEmployee => {
        dispatcher.dispatch({
            actionType: employee.id
                ? actionTypes.UPDATE_EMPLOYEE
                : actionTypes.CREATE_EMPLOYEE,
            employee: savedEmployee
        });
    });
}

export function loadEmployees() {
    return ajaxGet("http://localhost:8080/api/employee").then(employees => {
        dispatcher.dispatch({
            actionType: actionTypes.LOAD_EMPLOYEES,
            employees: employees
        });
    });
}

export function deleteEmployee(id : any) {
    return ajaxDelete("http://localhost:8080/api/employee", id).then(() => {
        dispatcher.dispatch({
            actionType: actionTypes.DELETE_EMPLOYEE,
            id : id
        });
    });
}