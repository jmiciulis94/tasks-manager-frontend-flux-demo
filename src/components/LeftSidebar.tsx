import React from "react";

function LeftSidebar() {
    return (
        <>
        <aside className="left-sidebar">
            <div className="scroll-sidebar">
                <div className="user-profile">
                    <div className="profile-img">
                        <img src="../../assets/images/users/1.jpg" alt="user"/>
                    </div>
                    <div className="profile-text">
                        <a href="#" className="dropdown-toggle link u-dropdown" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="true">
                            Markarn Doe
                            <span className="caret"></span>
                        </a>
                        <div className="dropdown-menu animated flipInY">
                            <a href="#" className="dropdown-item"><i className="ti-user"></i> My Profile</a>
                            <a href="#" className="dropdown-item"><i className="ti-wallet"></i> My Balance</a>
                            <div className="dropdown-divider"></div>
                            <a href="#" className="dropdown-item"><i className="ti-settings"></i> Account Setting</a>
                            <div className="dropdown-divider"></div>
                            <a href="login.html" className="dropdown-item"><i
                                className="fa fa-power-off"></i> Logout</a>
                        </div>
                    </div>
                </div>
                <nav className="sidebar-nav">
                    <ul id="sidebarnav">
                        <li className="nav-small-cap">MAIN</li>
                        <li>
                            <a href="javascript:void(0)" aria-expanded="false">
                                <i className="mdi mdi-gauge"></i>
                                <span className="hide-menu">Dashboard </span>
                            </a>
                        </li>
                        <li className="nav-small-cap">DATA</li>
                        <li>
                            <a href="/employee" aria-expanded="false">
                                <i className="mdi mdi-account-circle"></i>
                                <span className="hide-menu">Employees</span>
                            </a>
                        </li>
                        <li>
                            <a className="has-arrow " href="#" aria-expanded="false">
                                <i className="mdi mdi-briefcase"></i>
                                <span className="hide-menu">Projects</span>
                            </a>
                            <ul aria-expanded="false" className="collapse">
                                <li>
                                    <a href="/project">Skalvių Vilos</a>
                                </li>
                                <li>
                                    <a href="/project">Šilo Namai</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0)" aria-expanded="false">
                                <i className="mdi mdi-calendar-multiple-check"></i>
                                <span className="hide-menu">Timecards</span>
                            </a>
                        </li>
                        <li className="nav-small-cap">REPORTS</li>
                        <li>
                            <a href="javascript:void(0)" aria-expanded="false">
                                <i className="mdi mdi-calculator"></i>
                                <span className="hide-menu">By Project</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" aria-expanded="false">
                                <i className="mdi mdi-clipboard-account"></i>
                                <span className="hide-menu">By Employee</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" aria-expanded="false">
                                <i className="mdi mdi-calendar-clock"></i>
                                <span className="hide-menu">By Month</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div className="sidebar-footer">
                <a href="" className="link" data-toggle="tooltip" title="Settings"><i className="ti-settings"></i></a>
                <a href="" className="link" data-toggle="tooltip" title="Email"><i className="mdi mdi-gmail"></i></a>
                <a href="" className="link" data-toggle="tooltip" title="Logout"><i className="mdi mdi-power"></i></a>
            </div>
        </aside>
        </>
    );
}

export default LeftSidebar;