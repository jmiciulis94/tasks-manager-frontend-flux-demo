import React from "react";
import { NavLink } from "react-router-dom";

const Navbar = () => {

    const activeStyle = {color: "orange"};

    return (
        <nav>
            <h4 className="mt-3">
                <NavLink activeStyle={activeStyle} to="/employees">Employees</NavLink>
                {" | "}
                <NavLink activeStyle={activeStyle} to="/managers">Managers</NavLink>
            </h4>
        </nav>
    );
}
export default Navbar;