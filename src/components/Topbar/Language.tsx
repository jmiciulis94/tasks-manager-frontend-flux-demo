import React from "react";

function Language() {
    return (
        <>
            <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle text-muted waves-effect waves-dark" href=""
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i className="flag-icon flag-icon-us"></i>
                </a>
                <div className="dropdown-menu  dropdown-menu-right animated bounceInDown">
                    <a className="dropdown-item" href="#">
                        <i className="flag-icon flag-icon-in"></i>
                        India
                    </a>
                    <a className="dropdown-item" href="#">
                        <i className="flag-icon flag-icon-fr"></i>
                        French
                    </a>
                    <a className="dropdown-item" href="#">
                        <i className="flag-icon flag-icon-cn"></i>
                        China
                    </a>
                    <a className="dropdown-item" href="#">
                        <i className="flag-icon flag-icon-de"></i>
                        Dutch
                    </a>
                </div>
            </li>
        </>
    );
}

export default Language;