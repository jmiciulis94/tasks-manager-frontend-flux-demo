import React from "react";

function Logo() {
    return (
        <>
            <div className="navbar-header">
                <a className="navbar-brand" href="/">
                    <b>
                        <img src="../../assets/images/logo-icon.png" alt="homepage" className="dark-logo"/>
                    </b>
                    <span>
                        <img src="../../assets/images/logo-text.png" alt="homepage"
                             className="dark-logo"/>
                    </span>
                </a>
            </div>
        </>
    );
}

export default Logo;