import React from "react";

function MegaDropDown() {
    return (
        <>
            <li className="nav-item dropdown mega-dropdown">
                <a className="nav-link dropdown-toggle text-muted waves-effect waves-dark" href=""
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i className="mdi mdi-view-grid"></i>
                </a>
                <div className="dropdown-menu animated bounceInDown">
                    <ul className="mega-dropdown-menu row">
                        <li className="col-lg-3 col-xlg-2 mb-4">
                            <h4 className="mb-3">Smart Tech Solutions</h4>
                            <div id="carouselExampleControls" className="carousel slide"
                                 data-ride="carousel">
                                <div className="carousel-inner" role="listbox">
                                    <div className="carousel-item active">
                                        <div className="container"><img className="d-block img-fluid"
                                                                        src="../../assets/images/big/img1.jpg"
                                                                        alt="First slide"></img></div>
                                    </div>
                                    <div className="carousel-item">
                                        <div className="container"><img className="d-block img-fluid"
                                                                        src="../../assets/images/big/img2.jpg"
                                                                        alt="Second slide"></img></div>
                                    </div>
                                    <div className="carousel-item">
                                        <div className="container"><img className="d-block img-fluid"
                                                                        src="../../assets/images/big/img3.jpg"
                                                                        alt="Third slide"></img></div>
                                    </div>
                                </div>
                                <a className="carousel-control-prev" href="#carouselExampleControls"
                                   role="button" data-slide="prev">
                                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span className="sr-only">Previous</span>
                                </a>
                                <a className="carousel-control-next" href="#carouselExampleControls"
                                   role="button" data-slide="next">
                                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span className="sr-only">Next</span>
                                </a>
                            </div>
                        </li>
                        <li className="col-lg-3 mb-4">
                            <h4 className="mb-3">Our products</h4>
                            <div id="accordion" className="nav-accordion" role="tablist"
                                 aria-multiselectable="true">
                                <div className="card">
                                    <div className="card-header" role="tab" id="headingOne">
                                        <h5 className="mb-0">
                                            <a data-toggle="collapse" data-parent="#accordion"
                                               href="#collapseOne" aria-expanded="true"
                                               aria-controls="collapseOne">Poker Tournament Manager</a>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" className="collapse show" role="tabpanel"
                                         aria-labelledby="headingOne">
                                        <div className="card-body">This product allows you to create and
                                            manager large scale poker tournaments.
                                        </div>
                                    </div>
                                </div>
                                <div className="card">
                                    <div className="card-header" role="tab" id="headingTwo">
                                        <h5 className="mb-0">
                                            <a className="collapsed" data-toggle="collapse"
                                               data-parent="#accordion" href="#collapseTwo"
                                               aria-expanded="false" aria-controls="collapseTwo">Tasks
                                                Manager</a>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" className="collapse" role="tabpanel"
                                         aria-labelledby="headingTwo">
                                        <div className="card-body">This product allows you to manage your
                                            projects and finances.
                                        </div>
                                    </div>
                                </div>
                                <div className="card">
                                    <div className="card-header" role="tab" id="headingThree">
                                        <h5 className="mb-0">
                                            <a className="collapsed" data-toggle="collapse"
                                               data-parent="#accordion" href="#collapseThree"
                                               aria-expanded="false" aria-controls="collapseThree">Money
                                                Manager</a>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" className="collapse" role="tabpanel"
                                         aria-labelledby="headingThree">
                                        <div className="card-body">This product allows you to plan and
                                            manage your personal finances.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li className="col-lg-3  mb-4">
                            <h4 className="mb-3">Contact Us</h4>
                            <form>
                                <div className="form-group">
                                    <input type="text" className="form-control" id="exampleInputname1"
                                           placeholder="Enter Name"></input></div>
                                <div className="form-group">
                                    <input type="email" className="form-control" id="exampleInputEmail1"
                                           placeholder="Enter email"></input></div>
                                <div className="form-group">
                                                <textarea className="form-control" id="exampleTextarea" data-rows="3"
                                                          placeholder="Question or message"></textarea>
                                </div>
                                <button type="submit" className="btn btn-info">Submit</button>
                            </form>
                        </li>
                        <li className="col-lg-3 col-xlg-4 mb-4">
                            <h4 className="mb-3">Links</h4>
                            <ul className="list-style-none">
                                <li>
                                    <a href="javascript:void(0)">
                                        <i className="fa fa-check text-success"></i>
                                        About Us
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                        <i className="fa fa-check text-success"></i>
                                        FAQ
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                        <i className="fa fa-check text-success"></i>
                                        Pricing
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                        <i className="fa fa-check text-success"></i>
                                        Privacy Policy
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </li>
        </>
    );
}

export default MegaDropDown;