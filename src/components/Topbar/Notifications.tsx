import React from "react";

function Notifications() {
    return (
        <>
            <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark"
                   href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i className="mdi mdi-message"></i>
                    <div className="notify">
                        <span className="heartbit"></span>
                        <span className="point"></span>
                    </div>
                </a>
                <div className="dropdown-menu mailbox animated bounceInDown">
                    <ul>
                        <li>
                            <div className="drop-title">Notifications</div>
                        </li>
                        <li>
                            <div className="message-center">
                                <a href="#">
                                    <div className="btn btn-danger btn-circle">
                                        <i className="fa fa-link"></i>
                                    </div>
                                    <div className="mail-contnet">
                                        <h5>Version 1.0 released</h5>
                                        <span className="mail-desc">You are now able to create invoices for your clients.</span>
                                        <span className="time">9:30 AM</span>
                                    </div>
                                </a>
                                <a href="#">
                                    <div className="btn btn-success btn-circle">
                                        <i className="ti-calendar"></i>
                                    </div>
                                    <div className="mail-contnet">
                                        <h5>Your subscription is ending</h5>
                                        <span className="mail-desc">Your premium subscription will end 12/08/2020.</span>
                                        <span className="time">9:10 AM</span>
                                    </div>
                                </a>
                                <a href="#">
                                    <div className="btn btn-info btn-circle">
                                        <i className="ti-settings"></i>
                                    </div>
                                    <div className="mail-contnet">
                                        <h5>Privacy Settings</h5>
                                        <span
                                            className="mail-desc">Please review our privacy settings.</span>
                                        <span className="time">7:00 AM</span>
                                    </div>
                                </a>
                                <a href="#">
                                    <div className="btn btn-danger btn-circle">
                                        <i className="fa fa-link"></i>
                                    </div>
                                    <div className="mail-contnet">
                                        <h5>Version 0.9 released</h5>
                                        <span
                                            className="mail-desc">You are now able to see employee report.</span>
                                        <span className="time">5:30 AM</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li>
                            <a className="nav-link text-center" href="javascript:void(0);"> <strong>Check
                                all notifications</strong> <i className="fa fa-angle-right"></i> </a>
                        </li>
                    </ul>
                </div>
            </li>
        </>
    );
}

export default Notifications;