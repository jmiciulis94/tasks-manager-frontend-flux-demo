import React from "react";

function Search() {
    return (
        <>
            <li className="nav-item hidden-sm-down">
                <form className="app-search">
                    <input type="text" className="form-control" placeholder="Search for..."/>
                    <a className="srh-btn">
                        <i className="ti-search"></i>
                    </a>
                </form>
            </li>
        </>
    );
}

export default Search;