import React from "react";

function SidebarToggler() {
    return (
        <>
            <li className="nav-item">
                <a className="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark"
                   href="javascript:void(0)">
                    <i className="icon-arrow-left-circle"></i>
                </a>
            </li>
        </>
    );
}

export default SidebarToggler;