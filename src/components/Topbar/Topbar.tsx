import React from "react";

import Logo from "./Logo";
import SidebarToggler from "./SidebarToggler";
import Notifications from "./Notifications";
import MegaDropDown from "./MegaDropDown";
import Search from "./Search";
import UserProfile from "./UserProfile";
import Language from "./Language";

function Topbar() {
    return (
        <>
            <header className="topbar">
                <nav className="navbar top-navbar navbar-expand-md navbar-light">
                    <Logo />
                    <div className="navbar-collapse">
                        <ul className="navbar-nav mr-auto mt-md-0 ">
                            <SidebarToggler />
                            <Notifications />
                            <MegaDropDown />
                        </ul>
                        <ul className="navbar-nav my-lg-0">
                            <Search />
                            <UserProfile />
                            <Language />
                        </ul>
                    </div>
                </nav>
            </header>
        </>
    );
}

export default Topbar;