import React from "react";

function UserProfile() {
    return (
        <>
            <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle text-muted waves-effect waves-dark" href=""
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="../../assets/images/users/1.jpg" alt="user" className="profile-pic"/>
                </a>
                <div className="dropdown-menu dropdown-menu-right animated flipInY">
                    <ul className="dropdown-user">
                        <li>
                            <div className="dw-user-box">
                                <div className="u-img"><img src="../../assets/images/users/1.jpg" alt="user"></img>
                                </div>
                                <div className="u-text">
                                    <h4>Steave Jobs</h4>
                                    <p className="text-muted">varun@gmail.com</p>
                                    <a href="profile.html" className="btn btn-rounded btn-danger btn-sm">View
                                        Profile</a>
                                </div>
                            </div>
                        </li>
                        <li role="separator" className="divider"></li>
                        <li>
                            <a href="#"><i className="ti-user"></i> My Profile</a>
                        </li>
                        <li>
                            <a href="#"><i className="ti-wallet"></i> My Balance</a>
                        </li>
                        <li role="separator" className="divider"></li>
                        <li>
                            <a href="#"><i className="ti-settings"></i> Account Setting</a>
                        </li>
                        <li role="separator" className="divider"></li>
                        <li>
                            <a href="#"><i className="fa fa-power-off"></i> Logout</a>
                        </li>
                    </ul>
                </div>
            </li>
        </>
    );
}

export default UserProfile;