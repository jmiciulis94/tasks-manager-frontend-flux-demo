import { Manager } from "./manager";

export interface Client {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    deleted: boolean;
    manager: Manager;
}