import { PayType } from "./payType";
import { Manager } from "./manager";

export interface Employee {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
    speciality: string;
    dateOfBirth: string;
    joinDate: string;
    typeOfPay: PayType;
    rate: number;
    payPerHour: number;
    deleted: boolean;
    manager: Manager;
}
