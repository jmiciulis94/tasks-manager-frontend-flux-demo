import { User } from "./user";

export interface Manager {
    id: number;
    firstName: string;
    lastName: string;
    speciality: string;
    dateOfBirth: string;
    deleted: boolean;
    user: User;
}
