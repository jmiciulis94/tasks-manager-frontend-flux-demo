export enum PayType {
    RATE = 'RATE',
    HOURLY = 'HOURLY',
    NONE = 'NONE'
}