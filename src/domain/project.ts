import { Manager } from "./manager";
import { Client } from "./client";

export interface Project {
    id: number;
    name: string;
    description: string;
    estimateCost: number;
    createDate: string;
    deadlineDate: string;
    deleted: boolean;
    manager: Manager;
    client: Client;
}