import { Project } from "./project";

export interface Task {
    id: number;
    name: string;
    completeness: number;
    createDate: string;
    deadlineDate: string;
    deleted: boolean;
    project: Project;
}