import { PayType } from "./payType";
import { Task } from "./task";
import { Employee } from "./employee";

export interface Timecard {
    id: number;
    description: string;
    typeOfPay: PayType;
    rate: number;
    payPerHour: number;
    minutes: number;
    createDate: string;
    deleted: boolean;
    task: Task;
    employee: Employee;
}