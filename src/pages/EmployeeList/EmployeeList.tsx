import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import { ajaxGet } from '../../utils/apiUtils';
import employeeStore from '../../stores/employeeStore';
import EmployeeTable from './EmployeeTable';
import { loadEmployees, deleteEmployee } from "../../actions/employeeActions";

const EmployeeList = () => {
    // Be flux
    // const [employees, setEmployees] = useState([]);
    // Su flux
    const [employees, setEmployees] = useState(employeeStore.getEmployees());

    useEffect(() => {

        // Be flux
        // ajaxGet("http://localhost:8080/api/employee").then(_employee => setEmployees(_employee));

        // Su flux
        employeeStore.addChangeListener(onChange);
        if (employeeStore.getEmployees().length === 0) loadEmployees();
        return () => employeeStore.removeChangeListener(onChange);

    }, []);

    function onChange() {
        // @ts-ignore
        setEmployees(employeeStore.getEmployees());
    }

    return (
        <>
            <Link className="btn btn-primary mt-3 mb-3" to="/employee">
                Add Employee
            </Link>
            <div className="jumbotron">
                <h1>Employee List</h1>
            </div>
            <EmployeeTable employees={employees} deleteEmployee={deleteEmployee} />
        </>
    );
}

export default EmployeeList;