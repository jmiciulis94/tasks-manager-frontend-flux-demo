import React from "react";
import { Employee } from '../../domain/employee';
import { Link } from "react-router-dom";
import { ageFromDateOfBirth } from '../../utils/dateUtils';
import {phoneFormatter} from "../../utils/phoneUtils";

export type EmployeeTableProps = {
    employees: Employee[];
    deleteEmployee: any;
};

const EmployeeTable = ({employees, deleteEmployee}: EmployeeTableProps) => {

    function renderRow(employee: Employee) {

        // iskelti i payTypeUtils?
        const payType = (type: string) => {
            switch(type) {
                case "RATE":
                    return employee.rate;
                case "HOURLY":
                    return employee.payPerHour + "€/hr";
                case "NONE":
                    return "0€/hr"
                default:
                    return "unknown";
            }
        }

        if (!employee.deleted) {
            return (
                <tr key={employee.id}>
                    <td>
                        <Link to={"/employee/" + employee.id}>{employee.id}</Link>
                    </td>
                    <td>
                        <Link to={"/employee/" + employee.id}>{employee.firstName + " " + employee.lastName}</Link>
                    </td>
                    <td>
                        <Link to={"/employee/" + employee.id}>{employee.email}</Link>
                    </td>
                    <td>
                        <Link to={"/employee/" + employee.id}>{phoneFormatter(employee.phone)}</Link>
                    </td>
                    <td>
                        <Link to={"/employee/" + employee.id}>{employee.speciality}</Link>
                    </td>
                    <td>
                        <Link to={"/employee/" + employee.id}>{ageFromDateOfBirth(employee.dateOfBirth)}</Link>
                    </td>
                    <td>
                        <Link to={"/employee/" + employee.id}>{employee.joinDate}</Link>
                    </td>
                    <td>
                        <Link to={"/employee/" + employee.id}>{employee.typeOfPay}</Link>
                    </td>
                    <td>
                        <Link to={"/employee/" + employee.id}>{payType(employee.typeOfPay)}</Link>
                    </td>
                    <td>
                        <button
                            className="btn btn-danger mt-1 mb-1"
                            onClick={() => deleteEmployee(employee.id)}
                        >
                            Delete
                        </button>
                    </td>
                </tr>
            );
        }
        else
            return;
    }

    return (
        <table className="table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Speciality</th>
                    <th>Age</th>
                    <th>Joining date</th>
                    <th>Type of pay</th>
                    <th>Rate</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {employees.map(renderRow)}
            </tbody>
        </table>
    );
}

export default EmployeeTable;
