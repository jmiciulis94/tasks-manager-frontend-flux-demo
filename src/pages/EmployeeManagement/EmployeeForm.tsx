import React from "react";
import TextInput from '../../components/TextInput';
import { Employee } from "../../domain/employee";
import { PayType } from "../../domain/payType";
import { Manager } from "../../domain/manager";

export type EmployeeFormProps = {
    employee: Employee,
    managers: Manager[],
    onChange: (event: React.FormEvent<HTMLInputElement> | React.FormEvent<HTMLSelectElement>) => void,
    onSubmit: (event: React.FormEvent<HTMLFormElement>) => void
};

const EmployeeForm = ({ employee, managers, onChange, onSubmit }: EmployeeFormProps) => {

    return (
        <form onSubmit={onSubmit}>

            <TextInput
                id="firstName"
                name="firstName"
                label="First name"
                onChange={onChange}
                value={employee.firstName}
            />

            <TextInput
                id="lastName"
                name="lastName"
                label="Last name"
                onChange={onChange}
                value={employee.lastName}
            />

            <div className="form-group">
                <label htmlFor="email">Email</label>
                <div className="field">
                    <input
                        id="email"
                        type="email"
                        name="email"
                        value={employee.email}
                        onChange={onChange}
                        className="form-control-6"
                    />
                </div>
            </div>

            <div className="form-group">
                <label htmlFor="phone">Phone number</label>
                <div className="field">
                    <input
                        id="phone"
                        type="tel"
                        name="phone"
                        value={employee.phone}
                        onChange={onChange}
                        className="form-control-6"
                    />
                </div>
            </div>

            <TextInput
                id="speciality"
                name="speciality"
                label="Speciality"
                onChange={onChange}
                value={employee.speciality}
            />

            <div className="form-group">
                <label htmlFor="dateOfBirth">Date of birth</label>
                <div className="field">
                    <input
                        id="dateOfBirth"
                        type="date"
                        name="dateOfBirth"
                        value={employee.dateOfBirth}
                        onChange={onChange}
                        className="form-control-6"
                    />
                </div>
            </div>

            <div className="form-group">
                <label htmlFor="typeOfPay">Type of pay</label>
                <div className="field">
                    <select
                        id="typeOfPay"
                        name="typeOfPay"
                        onChange={onChange}
                        value={employee.typeOfPay}
                        className="form-control-6"
                    >
                        <option value={PayType.NONE}>{PayType.NONE}</option>
                        <option value={PayType.HOURLY}>{PayType.HOURLY}</option>
                        <option value={PayType.RATE}>{PayType.RATE}</option>
                    </select>
                </div>
            </div>

            <div className="form-group">
                <label htmlFor="rate">Rate</label>
                <div className="field">
                    <input
                        id="rate"
                        type="number"
                        step="0.01"
                        name="rate"
                        value={employee.rate}
                        onChange={onChange}
                        className="form-control-6"
                    />
                </div>
            </div>

            <div className="form-group">
                <label htmlFor="payPerHour">Pay per hour(€)</label>
                <div className="field">
                    <input
                        id="payPerHour"
                        type="number"
                        step="1"
                        name="payPerHour"
                        value={employee.payPerHour}
                        onChange={onChange}
                        className="form-control-6"
                    />
                </div>
            </div>

            <div className="form-group">
                <label htmlFor="manager">Manager</label>
                <div className="field">
                    <select
                        id="manager"
                        name="manager"
                        onChange={onChange}
                        className="form-control-6"
                    >
                        <option value="" hidden/>
                        {managers.map((manager) =>
                            <option key={manager.id} value={manager.id}>
                                {manager.firstName + " " + manager.lastName}
                            </option>
                        )}
                    </select>
                </div>
            </div>

            <input type="submit" value="Save" className="btn btn-primary mb-5"/>
        </form>
    );
}

export default EmployeeForm;