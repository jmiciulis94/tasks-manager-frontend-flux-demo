import React, { useEffect, useState } from 'react';
import { RouteComponentProps } from "react-router";
import { ajaxGet, ajaxPost } from '../../utils/apiUtils';
import EmployeeForm from './EmployeeForm';
import { Employee } from "../../domain/employee";
import { toast } from "react-toastify";
import { PayType } from "../../domain/payType";
import employeeStore from "../../stores/employeeStore";
import * as employeeActions from "../../actions/employeeActions";

export type EmployeeManagementParams = {
    id: string;
};

const EmployeeManagement = (props: RouteComponentProps<EmployeeManagementParams>) => {

    const [employees, setEmployees] = useState(employeeStore.getEmployees());
    const [employee, setEmployee] = useState<Employee>({
        id: 0,
        firstName: "",
        lastName: "",
        email: "",
        phone: "",
        speciality: "",
        dateOfBirth: "",
        joinDate: "",
        typeOfPay: PayType.NONE,
        rate: 0,
        payPerHour: 0,
        deleted: false,
        manager: {
            id: 0,
            firstName: "",
            lastName: "",
            speciality: "",
            dateOfBirth: "",
            deleted: false,
            user: {
                id: 0,
                username: "",
                password: "",
                email: ""
            }
        }
    });
    const [managers, setManagers] = useState([]);

    useEffect(() => {
        employeeStore.addChangeListener(onChange);
        const id: number = parseInt(props.match.params.id);
        if (employees.length === 0) {
            employeeActions.loadEmployees();
        } else if (id) {
            // be Flux
            // ajaxGet("http://localhost:8080/api/employee/" + id).then((_employee) => {
            //     setEmployee(_employee);
            // });

            // su Flux
            // @ts-ignore
            setEmployee(employeeStore.getEmployeeById(id));
        }
        return () => employeeStore.removeChangeListener(onChange);
    }, [employees.length, props.match.params.id]);

    function onChange() {
        setEmployees(employeeStore.getEmployees());
    }

    useEffect(() => {
        ajaxGet("http://localhost:8080/api/manager").then(_manager => setManagers(_manager));
    }, []);

    const handleChange = (event: React.FormEvent<HTMLInputElement> | React.FormEvent<HTMLSelectElement>) => {
        const id: number = parseInt(event.currentTarget.value);
        // @ts-ignore
        setEmployee({...employee, [event.currentTarget.name]: event.currentTarget.value, manager: managers.find(manager => manager.id === id)
        });
    };

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        // Be flux
        // return ajaxPost("http://localhost:8080/api/employee", employee).then(() => {
        //     props.history.push("/employees")
        //     toast.success("Employee saved");
        // });

        // Su flux
        employeeActions.saveEmployee(employee).then(() => {
            props.history.push("/employees")
            toast.success("Employee saved");
        });

    };

    return (
        <>
            <h2>Employee</h2>
            <EmployeeForm employee={employee} managers={managers} onChange={handleChange} onSubmit={handleSubmit} />
        </>
    );
}

export default EmployeeManagement;