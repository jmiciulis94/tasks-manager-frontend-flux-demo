import React from "react";

const ExampleEmployeeList = () => {
    return (
        <div className="page-wrapper">
            <div className="container-fluid">
                <div className="row page-titles">
                    <div className="col-md-6 col-8 align-self-center">
                        <h3 className="text-themecolor mb-0 mt-0">Data</h3>
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item">
                                <a href="javascript:void(0)">Home</a>
                            </li>
                            <li className="breadcrumb-item active">Employees</li>
                        </ol>
                    </div>
                    <div className="col-md-6 col-4 align-self-center">
                        <button className="btn float-right hidden-sm-down btn-success" data-toggle="modal" data-target="#add-contact">
                            <i className="mdi mdi-plus-circle"></i>
                            Create Employee
                        </button>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">Employees</h4>
                                <h6 className="card-subtitle"></h6>
                                <div id="add-contact" className="modal fade in" tabIndex={-1} role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div className="modal-dialog">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <h4 className="modal-title" id="myModalLabel">Create New Employee</h4>
                                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div className="modal-body">
                                                <form className="floating-labels">
                                                    <div className="row pt-3 mb-4">
                                                        <div className="col-md-6">
                                                            <img src="../assets/images/users/4.jpg" className="img-responsive thumbnail rounded mr-3" />
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="fileupload btn btn-danger btn-rounded waves-effect waves-light">
                                                                    <span>
                                                                        <i className="ion-upload m-r-5"></i>
                                                                        Upload Photo
                                                                    </span>
                                                                <input type="file" className="upload" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row pt-3">
                                                        <div className="col-md-6">
                                                            <div className="form-group">
                                                                <input type="text" className="form-control" id="first-name" required />
                                                                <span className="bar"></span>
                                                                <label htmlFor="first-name">First Name</label>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="form-group">
                                                                <input type="text" className="form-control" id="last-name" required />
                                                                <span className="bar"></span>
                                                                <label htmlFor="last-name">Last Name</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row pt-3">
                                                        <div className="col-md-6">
                                                            <div className="form-group">
                                                                <input type="text" className="form-control" id="email" required />
                                                                <span className="bar"></span>
                                                                <label htmlFor="email">Email</label>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="form-group">
                                                                <input type="text" className="form-control" id="phone" required />
                                                                <span className="bar"></span>
                                                                <label htmlFor="phone">Phone Number</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row pt-3">
                                                        <div className="col-md-6">
                                                            <div className="form-group">
                                                                <select className="form-control" id="speciality" required>
                                                                    <option></option>
                                                                    <option>Betonuotojas</option>
                                                                    <option>Dažytojas</option>
                                                                    <option>Mūrininkas</option>
                                                                    <option>Plytelių klojėjas</option>
                                                                    <option>Santechnikas</option>
                                                                    <option>Vadovas</option>
                                                                </select>
                                                                <span className="bar"></span>
                                                                <label htmlFor="speciality">Speciality</label>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <input type="text" className="form-control" id="date-of-birth" required></input>
                                                                        <div className="input-group-append">
                                                                            <span className="input-group-text">
                                                                                <i className="icon-calender"></i>
                                                                            </span>
                                                                        </div>
                                                                        <span className="bar"></span>
                                                                        <label htmlFor="date-of-birth">Date of Birth</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row pt-3">
                                                        <div className="col-md-6">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <input type="text" className="form-control" id="start-date" required></input>
                                                                        <div className="input-group-append">
                                                                            <span className="input-group-text">
                                                                                <i className="icon-calender"></i>
                                                                            </span>
                                                                        </div>
                                                                        <span className="bar"></span>
                                                                        <label htmlFor="start-date">Start Date</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <input type="text" className="form-control" id="end-date" required></input>
                                                                        <div className="input-group-append">
                                                                            <span className="input-group-text">
                                                                                <i className="icon-calender"></i>
                                                                            </span>
                                                                        </div>
                                                                        <span className="bar"></span>
                                                                        <label htmlFor="end-date">End Date</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row pt-3">
                                                        <div className="col-md-6">
                                                            <div className="form-group">
                                                                <select className="form-control" id="salary" required>
                                                                    <option></option>
                                                                    <option>Paid/Hour</option>
                                                                    <option>Ratio</option>
                                                                </select>
                                                                <span className="bar"></span>
                                                                <label htmlFor="salary">Salary</label>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="form-group">
                                                                <input type="text" className="form-control" id="paid-per-hour" required />
                                                                <span className="bar"></span>
                                                                <label htmlFor="paid-per-hour">Paid per Hour</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div className="modal-footer">
                                                <button type="button" className="btn btn-info waves-effect" data-dismiss="modal">Save</button>
                                                <button type="button" className="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="table-responsive">
                                    <table id="demo-foo-addrow" className="table table-bordered m-t-30 table-hover contact-list" data-paging="true" data-paging-size="7">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Full Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Speciality</th>
                                            <th>Age</th>
                                            <th>Joining date</th>
                                            <th>Rate</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                <a href="javascript:void(0)">
                                                    <img src="../assets/images/users/5.jpg" alt="user" width="40" className="img-circle" />
                                                    Marius Guobys
                                                </a>
                                            </td>
                                            <td>genelia@gmail.com</td>
                                            <td>+370 643 63215</td>
                                            <td>
                                                <span className="label label-danger">Dažytojas</span>
                                            </td>
                                            <td>23</td>
                                            <td>2014-12-10</td>
                                            <td>12 €/h</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>
                                                <a href="javascript:void(0)">
                                                    <img src="../assets/images/users/5.jpg" alt="user" width="40" className="img-circle" />
                                                    Marius Triukas
                                                </a>
                                            </td>
                                            <td>marius.triukas@gmail.com</td>
                                            <td>+370 643 56789</td>
                                            <td>
                                                <span className="label label-info">Plytelių klojėjas</span>
                                            </td>
                                            <td>26</td>
                                            <td>2014-10-09</td>
                                            <td>17 €/h</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>
                                                <a href="javascript:void(0)">
                                                    <img src="../assets/images/users/6.jpg" alt="user" width="40" className="img-circle" />
                                                    Vidas Timukas
                                                </a>
                                            </td>
                                            <td>vidas.timukas@gmail.com</td>
                                            <td>+370 643 62147</td>
                                            <td>
                                                <span className="label label-success">Mūrininkas</span>
                                                <br />
                                                <span className="label label-info">Plytelių klojėjas</span>
                                            </td>
                                            <td>28</td>
                                            <td>2013-01-10</td>
                                            <td>0.7</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>
                                                <a href="javascript:void(0)">
                                                    <img src="../assets/images/users/7.jpg" alt="user" width="40" className="img-circle" />
                                                    Rūtenis Turčinas
                                                </a>
                                            </td>
                                            <td>rturcinas@gmail.com</td>
                                            <td>+370 643 32145</td>
                                            <td>
                                                <span className="label label-inverse">Santechnikas</span>
                                            </td>
                                            <td>25</td>
                                            <td>2019-02-10</td>
                                            <td>22 €/h</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>
                                                <a href="javascript:void(0)">
                                                    <img src="../assets/images/users/6.jpg" alt="user" width="40" className="img-circle" />
                                                    Mindaugas Radžius
                                                </a>
                                            </td>
                                            <td>minrad@gmail.com</td>
                                            <td></td>
                                            <td>
                                                <span className="label label-danger">Vadovas</span>
                                                <br />
                                                <span className="label label-inverse">Santechnikas</span>
                                            </td>
                                            <td>23</td>
                                            <td>2015-10-09</td>
                                            <td>0.4</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>
                                                <a href="javascript:void(0)">
                                                    <img src="../assets/images/users/6.jpg" alt="user" width="40" className="img-circle" />
                                                    Marius Malcius
                                                </a>
                                            </td>
                                            <td>marius.malcius@gmail.com</td>
                                            <td>+370 643 62142</td>
                                            <td>
                                                <span className="label label-warning">Betonuotojas</span>
                                            </td>
                                            <td>29</td>
                                            <td>2013-10-05</td>
                                            <td>0.9</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>
                                                <a href="javascript:void(0)">
                                                    <img src="../assets/images/users/2.jpg" alt="user" width="40" className="img-circle" />
                                                    Daiva Brazukaitė
                                                </a>
                                            </td>
                                            <td>daiva.brazukaitė@gmail.com</td>
                                            <td></td>
                                            <td>
                                                <span className="label label-danger">Dažytojas</span>
                                            </td>
                                            <td>35</td>
                                            <td>2012-05-10</td>
                                            <td>18 €/h</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>
                                                <a href="javascript:void(0)">
                                                    <img src="../assets/images/users/5.jpg" alt="user" width="40" className="img-circle" />
                                                    Linas Naruškevičius
                                                </a>
                                            </td>
                                            <td>naruskevicius@gmail.com</td>
                                            <td>+370 643 32146</td>
                                            <td>
                                                <span className="label label-info">Plytelių klojėjas</span>
                                                <br />
                                                <span className="label label-danger">Dažytojas</span>
                                                <br />
                                                <span className="label label-success">Mūrininkas</span>
                                            </td>
                                            <td>27</td>
                                            <td>2014-11-10</td>
                                            <td>1.0</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>
                                                <a href="javascript:void(0)">
                                                    <img src="../assets/images/users/6.jpg" alt="user" width="40" className="img-circle" />
                                                    Vaidas Semėnas
                                                </a>
                                            </td>
                                            <td>vaidas@gmail.com</td>
                                            <td>+370 643 98564</td>
                                            <td>
                                                <span className="label label-success">Mūrininkas</span>
                                            </td>
                                            <td>18</td>
                                            <td>2019-12-05</td>
                                            <td>25 €/h</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>
                                                <a href="javascript:void(0)">
                                                    <img src="../assets/images/users/5.jpg" alt="user" width="40" className="img-circle" />
                                                    Mantas Gulbinas
                                                </a>
                                            </td>
                                            <td>mantas.gulbinas@gmail.com</td>
                                            <td>+370 643 55124</td>
                                            <td>
                                                <span className="label label-inverse">Santechnikas</span>
                                            </td>
                                            <td>36</td>
                                            <td>2009-08-15</td>
                                            <td>0.8</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>
                                                <a href="javascript:void(0)">
                                                    <img src="../assets/images/users/8.jpg" alt="user" width="40" className="img-circle" />
                                                    Indrė Čepurnienė
                                                </a>
                                            </td>
                                            <td>icepurniene@gmail.com</td>
                                            <td>+370 643 32151</td>
                                            <td>
                                                <span className="label label-danger">Vadovas</span>
                                                <br />
                                                <span className="label label-inverse">Santechnikas</span>
                                            </td>
                                            <td>43</td>
                                            <td>2010-12-10</td>
                                            <td>0.7</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>
                                                <a href="javascript:void(0)">
                                                    <img src="../assets/images/users/6.jpg" alt="user" width="40" className="img-circle" />
                                                    Pavelas Beleckas
                                                </a>
                                            </td>
                                            <td>pavelas.beleckas@gmail.com</td>
                                            <td>+370 643 31542</td>
                                            <td>
                                                <span className="label label-danger">Dažytojas</span>
                                            </td>
                                            <td>23</td>
                                            <td>2014-12-10</td>
                                            <td>14 €/h</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>
                                                <a href="javascript:void(0)">
                                                    <img src="../assets/images/users/7.jpg" alt="user" width="40" className="img-circle" />
                                                    Rimvydas Paškevičius
                                                </a>
                                            </td>
                                            <td>rimvydas.paskevicius@gmail.com</td>
                                            <td>+370 643 35400</td>
                                            <td>
                                                <span className="label label-info">Plytelių klojėjas</span>
                                            </td>
                                            <td>26</td>
                                            <td>2014-10-09</td>
                                            <td>18 €/h</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>
                                                <a href="javascript:void(0)">
                                                    <img src="../assets/images/users/5.jpg" alt="user" width="40" className="img-circle" />
                                                    Justas Eimutis
                                                </a>
                                            </td>
                                            <td>justas.eimutis@gmail.com</td>
                                            <td></td>
                                            <td>
                                                <span className="label label-success">Mūrininkas</span>
                                            </td>
                                            <td>28</td>
                                            <td>2013-01-10</td>
                                            <td>0.6</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer className="footer">
                Copyright © 2020 Smart Tech Solutions
            </footer>
        </div>
    );
}

export default ExampleEmployeeList;