import { EventEmitter } from 'events';
import Dispatcher from '../appFluxDispatcher';
import actionTypes from '../actions/actionTypes';
import { Employee } from "../domain/employee";

const CHANGE_EVENT = "change";
let _employees : Employee[] = [];

class EmployeeStore extends EventEmitter {
    addChangeListener(callback : any) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback : any) {
        this.removeListener(CHANGE_EVENT, callback);
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    getEmployees() {
        return _employees;
    }

    getEmployeeById(id : number) {
        return _employees.find(employee => employee.id === id);
    }
}

const store = new EmployeeStore();

Dispatcher.register(action => {
    // @ts-ignore
    switch(action.actionType) {
        case actionTypes.DELETE_EMPLOYEE:
            // @ts-ignore
            _employees = _employees.filter(employee => employee.id !== parseInt(action.id, 10));
            store.emitChange();
            break;
        case actionTypes.CREATE_EMPLOYEE:
            // @ts-ignore
            _employees.push(action.employee);
            store.emitChange();
            break;
        case actionTypes.UPDATE_EMPLOYEE:
            // @ts-ignore
            _employees = _employees.map(employee => employee.id === action.employee.id ? action.employee : employee);
            store.emitChange();
            break;
        case actionTypes.LOAD_EMPLOYEES:
            // @ts-ignore
            _employees = action.employees;
            store.emitChange();
            break;
        default:
            // nothing to do here
    }
});

export default store;