export function ajaxGet(url: string): Promise<any> {
    const options: RequestInit = {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
        }
    };
    return ajax(url, options);
}

export function ajaxPost(url: string, data: any): Promise<any> {
    const options: RequestInit = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
        }
    };
    return ajax(url, options);
}

export function ajaxDelete(url: string, id: any): Promise<any> {
    url = url + "/" + id;
    const options: RequestInit = {
        method: "DELETE"
    };
    return ajax(url, options);
}

function ajax(url: string, options: RequestInit): Promise<any> {
    return new Promise((resolve, reject) => {
        fetch(url, options).then(response => response.json().then(json => ({
                status: response.status,
                json
            })
        )).then(response => {
            if (response.status >= 400) {
                reject(response.json);
            } else {
                resolve(response.json);
            }
        }, error => reject(error));
    });
}
