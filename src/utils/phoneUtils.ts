export function phoneFormatter(phone : any) : string {
    let phoneNumber = phone.substring(0,4) + " " + phone.substring(4,7) + " " + phone.substring(7,12)
    return phoneNumber;
}